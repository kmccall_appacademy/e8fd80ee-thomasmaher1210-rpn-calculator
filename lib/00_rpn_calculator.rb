class RPNCalculator

  def initialize(stack = [])
    @stack = stack
  end

  def push(n)
    @stack << n
  end

  def plus
    operation(:+)
  end

  def minus
    operation(:-)
  end

  def times
    operation(:*)
  end

  def divide
    operation(:/)
  end

  def value
    @stack[-1]
  end

  def operation(op)

    p @stack.length
    if @stack.length < 2
      raise "calculator is empty"
    end

    num2 = @stack.pop.to_f
    num1 = @stack.pop.to_f

    if op == :+
      @stack.push(num1+num2)
    elsif op == :-
      @stack.push(num1-num2)
    elsif op == :/
      @stack.push(num1/num2)
    elsif op == :*
      @stack.push(num1*num2)
    end
  end
  
end
